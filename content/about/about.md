---
title: "About"
date: 2022-11-30T15:04:46-06:00
draft: false
---

Hey, I'm Kieran👋

Machine Learning Engineer

Developing solutions since 2021  
Writing since 2019  
Triathlon, 3D Printing & Robotics enthusiast  
Email me at **hey** [at] **kmml.dev**

I really like statistical algorithms that can generalise tasks. or, Machine Learning.

My main programming language is Python.  
I can also get things done in C++ and Rust.

Solid experience developing solutions from concept to deployment, leveraging various ML libraries such as TensorFlow, Keras & PyTorch.

Effective developing scalable ML solutions for the Cloud (especially AWS & GCP).  
I also get things done with embedded ML in compute constrained problems.

## Work Experience

{{< tabs tabTotal="2" >}}

{{% tab tabName="Summary" %}}
- Mar 22 - Present: Machine Learning Engineer at Zuto
- Jun 21 - Mar 22: Data Scientist at Intelesant
- Jun 21 - Sep 22: Data Scientist (M.Sc.) at Gousto
{{% /tab %}}

{{% tab tabName="Detailed" %}}  
  
- Mar 22 - Present: Machine Learning Engineer at Zuto  
At Zuto, developed several concepts and improved company awareness of ML. 
The first ML hire, contributed to creating a solid development team and mentored others.

- Jun 21 - Mar 22: Data Scientist at Intelesant  
At the same time, until 2022 I developed IoT ML solutions with Intelesant, streaming events from zigbee devices and 
creating actionable insight at scale for NHS partners. Learned how to write maintainable software, collaborate with my team
and simplify product shipping to increase product value quickly.

- Jun 21 - Sep 21: Data Scientist (M.Sc.) at Gousto  
In 2021, as part of my M.Sc. at Lancaster, as a project for Gousto, I researched avenues for throughput analysis on 
their production lines. Learned to deliver explainable results to stakeholders & collaborate with a multi-functional team.

  

{{% /tab %}}  

{{< /tabs >}}



