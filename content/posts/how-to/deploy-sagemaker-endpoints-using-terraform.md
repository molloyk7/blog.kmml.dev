---
title: "Deploying SageMaker Endpoints using Terraform"
date: 2023-03-05T17:06:23Z
draft: true
image: brand_image.jpg
tags: ["how-to", "2023", "aws", "sagemaker", "terraform"]
---