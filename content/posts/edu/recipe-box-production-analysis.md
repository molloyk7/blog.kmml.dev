---
title: "MSc Thesis - Recipe Box Production Planning"
date: 2021-09-15T10:58:24Z
draft: false
image: brand_image.jpg
tags: ["python"]
series: "msc"
---

Nothing to see yet.
Although probably my proudest achievement to date.
Also doubles as an introduction to lots of concepts applied in other work.


But check out the [poster]({{< paper "edu/msc-poster.pdf" >}}), the [slides]({{< paper "edu/msc-slides.pdf" >}}), or the [thesis]({{< paper "edu/msc-thesis.pdf" >}}). for more details.

