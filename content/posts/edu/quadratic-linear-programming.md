---
title: "Quadratic Linear Programming - Shop Allocation"
date: 2020-01-04T15:49:00Z
draft: false
image: brand_image.jpg
tags: ["operational-research", "mosel"]
series: "bsc"
---

When planning a new shopping mall, where should you place the most popular stores? Getting the layout right is crucial so friends can easily shop together across their favorite spots. I helped mathematically optimize this for a client using an integer programming approach.

The problem has some key details. There are 4 sought-after stores that each need to be assigned to one of 4 vacant mall locations. The distance between each spot is known. And based on market research, we have data on the estimated number of shoppers going from store 1 to store 2, from 1 to 3, 2 to 3, etc.

The goal is to minimize total walking distance weighted by customer traffic flows between the stores. So if 5,000 people may go from store 1 to store 3, we want those stores as close as possible to reduce aggregate foot travel.

I formulated this as a 0-1 quadratic programming model. The decision variables are simple: 1 if store A locates at spot #1, 0 otherwise. Additional constraints ensure only one store in any spot and each store gets just one spot.

The objective function captures the weighted distances nicely. It has terms like:
(Dist_Spot1,Spot4) * (Store1,Spot1) * (Store2,Spot4)

So if Store 1 is placed at Spot 1 and Store 2 at Spot 4, this distance contributes to the total cost to minimize.

Using MATLAB, I optimized over all feasible store-to-location assignments to identify the best options delivering equal minimum cost. The client can now strategically placeanchor stores knowing quantified tradeoffs!

To see more details, check out the [paper]({{< paper "edu/aor-ky3.pdf" >}}).

Disclaimer: This project was completed as part of my BSc in Mathematics at Manchester Metropolitan University. The project was supervised by Dr. Keith Yates and [Dr. Philip Sinclair](https://www.mmu.ac.uk/staff/profile/dr-philip-sinclair).  
This blog post is an LLM generated text, based upon the hand written report.