---
title: "Comparing approaches for Deep Learning Time Series Classification"
date: 2021-02-19T08:04:00Z
draft: false
image: brand_image.jpg
tags: ["deep", "time-series", "classification", "python"]
series: "msc"
---

## Introduction to Deep Learning
I recently explored using deep learning models for time series classification. But before getting into the details, let me provide some background on deep learning. Deep learning is a subset of machine learning that uses neural networks modeled after the human brain to learn from large amounts of data. Neural networks have an input layer, hidden layers, and an output layer. By adjusting the weights between layers during training, neural nets can recognize complex patterns and make predictions. Check out this video for a great intro to deep learning.

## Time Series Classification
I specifically looked at time series classification. A time series is a sequence of data points over time. Examples include stock prices, sensor data, sound waves. The goal of time series classification is to analyze these sequences and assign them to categories. This has applications in areas like medical diagnosis, speech recognition, and fault detection. See here for more background on time series data.

## Deep Learning Models

In my paper, I tested out five different deep neural network architectures for classifying electrocardiogram (ECG) time series data:

- Multi-layer perceptrons (MLPs) - The simplest deep learning model
- Convolutional neural networks (CNNs) - Good at finding patterns
- Echo state networks (ESNs) - Specialized for time series
- Fully convolutional networks (FCNs)
- Residual networks (ResNets) - Very deep models
I trained these models on four ECG datasets with different heart conditions.

## Results and Analysis
The results showed the Encoder and ResNet models performed the best and significantly outperformed the other methods. They were able to classify ECG data swiftly and accurately, even better than a human cardiologist! I also looked at statistical significance using critical difference diagrams. (What is a critical difference diagram, you ask. Check out the [article explaining them](https://mirkobunse.github.io/CriticalDifferenceDiagrams.jl/dev/) and the [library I made](/posts/lib/cdg/) especially for this - inspired from the [original python version](https://github.com/hfawaz/cd-diagram)) Check out my full paper for those details!

## Final Thoughts
In conclusion, deep learning shows real promise for automating the diagnosis of heart conditions using ECG signals. With more research into areas like data augmentation and transfer learning, we can continue improving these models. Exciting times ahead!
If I was to take a look at this work again, it would be to extend Echo State Networks - they were really cool.


Let me know if you have any other questions about my work in deep learning or time series analysis!

To see more details, check out the [paper]({{< paper "edu/scc413-1.pdf" >}})

Disclaimer: This project was completed as part of my MSc in Data Science Lancaster University.
This blog post is an LLM generated text, based upon the hand-written report.
