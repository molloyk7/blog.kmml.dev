---
title: "Production Planning"
date: 2020-01-03T11:50:00Z
draft: false
image: brand_image.jpg
tags: ["operational-research", "mosel"]
series: "bsc"
---
Remember the story of Goldilocks and the Three Bears? Papa Bear, Mama Bear, and Baby Bear each had their own bowl of porridge. Now imagine the Three Bears started a porridge company! As their assistant, I helped them determine the optimal production plan to meet customer demand over the year while minimizing costs.

This may sound like a fun fairy tale problem, but it draws on some sophisticated operations research concepts! Specifically, it's a multi-item production planning model with capacity constraints. Let me break that down...

First, the Three Bears make three porridge products: plain natural, sweet honey, and mixed fruit flavor. So I had to optimize production across multiple item types, not just a single product.

Second, I planned output over a full 12 month span. Demand for warm, cozy porridge isn't steady – it spikes in the colder months! My plan ensured enough seasonal supply without overproducing.

The trickiest part was the capacity limits. The Three Bears can only produce so much porridge per month given their big cooking pots and bear-power. And different staff have different hourly wages. I had to optimize the production schedule to never exceed their cooking capacity or labor budget.

I used the optimization power of linear programming to create monthly production quantities and workforce plans. The mathematical model keeps adjusting the possible solutions until it finds the very lowest cost way to satisfy all the porridge demand over the year.

In the end, I delivered the optimal plan to keep the Three Bears’ business running smoothly! Now Goldilocks can enjoy a nice bowl of their porridge without depleting their stock. Though I told Papa Bear to be careful who he lets in the house...

To see more details, check out the [paper]({{< paper "edu/aor-ky2.pdf" >}}).

Disclaimer: This project was completed as part of my BSc in Mathematics at Manchester Metropolitan University. The project was supervised by Dr. Keith Yates and [Dr. Philip Sinclair](https://www.mmu.ac.uk/staff/profile/dr-philip-sinclair).
This blog post is an LLM generated text, based upon the hand written report.