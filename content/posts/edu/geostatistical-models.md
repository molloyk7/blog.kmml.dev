---
title: "Geostatical Models"
date: 2021-05-13T16:53:00Z
draft: false
image: brand_image.jpg
tags: ["arcgis", "geostatistics", "python"]
series: "msc"
---

## Overview

The report describes efforts to map the prevalence and risk of onchocerciasis (river blindness) infection across Gabon in order to focus ivermectin treatment programs.
Onchocerciasis causes severe itching, skin lesions, and vision impairment. Over 99% of cases are in sub-Saharan Africa.
The analysis uses data collected on infection prevalence across 59 survey villages in Gabon, along with geographic data like elevation and water proximity.
Geographic information system (GIS) concepts and geostatistical analysis are used to estimate infection risk across Gabon. Specifically, kriging and empirical Bayesian kriging (EBK) models are developed.

## Key Data

Infection prevalence data collected via skin biopsies in 59 villages. Average prevalence of 1.84% across villages.
Geographic data includes elevation raster layer and water proximity (Euclidean distance to nearest water source) raster layer.
High risk threshold set at 5% prevalence based on previous surveys showing high risk above 20%, but maximal prevalence now only 11.8% after ivermectin treatment.

## Analysis Methods

Kriging: Interpolates prevalence values across space based on spatial covariance structure. Allows inclusion of elevation data. Key assumptions are stationarity and isotropy.
Empirical Bayesian Kriging (EBK): Extends kriging to account for non-stationarity. Automatically tunes hyperparameters. Uses restricted maximum likelihood for variogram estimation.
Cross-validation: Leave-one-out approach to evaluate model performance.

## Key Results

Kriging unsuitable due to poor handling of coastal boundaries.
EBK elevation model shows some high risk areas clustered inland. Matches observed data distributions.
EBK model with elevation + water proximity shows risk bands following rivers more closely. Larger high risk area.

## Future Work

Incorporate population density data to relate to prevalence or optimize treatment roll-out.


To see more details, check out the [paper]({{< paper "edu/chic563.pdf" >}}).


Disclaimer: This project was completed as part of my MSc in Data Science Lancaster University.
This blog post is an LLM generated text, based upon the hand-written report.