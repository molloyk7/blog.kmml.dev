---
title: "Partial Differential Equations: Higher Order Taylor Series Approximations"
date: 2020-01-03T11:50:00Z
draft: false
image: brand_image.jpg
tags: ["maths","matlab"]
series: "bsc"
---

In my applied mathematics work, I often want to approximate complex functions with simpler ones that are more tractable to analyze. One common technique is to use a Taylor series expansion. This approximates a function as an infinite sum of terms involving the function's derivatives at a point.

In my latest research, I investigated using higher order Taylor series, specifically up to the fourth derivative, to approximate the derivative of the cosine function. My motivation was to quantify the tradeoff between accuracy and computational expense as more derivative terms are included.

I computed numerical approximations using finite difference formulas – essentially estimating derivatives from nearby function values. Several methods were tried: forward (uses points to the right), backward (uses points to the left), second order central, and fourth order central.

The key result is that including more derivative terms dramatically improved my accuracy, despite marginally higher computational cost. For example, at the largest step size I tested (h=0.5), my fourth order approximation had 0.0017 absolute error, over 100 times smaller than my first order approximations!

As I decreased the step size, all my methods converged toward the exact value, but lower order approximations required much smaller h to achieve a given precision. This demonstrates to me the power of leveraging higher order derivatives when they are available.

My analysis also verified the expected order of accuracy of each of my methods using convergence rates. This checks how quickly error reduces as step size decreases, matching the order of the highest derivative term I included.

Overall, my research provides me a solid case study on the benefits of using higher order Taylor series. The improved efficiency in achieving numerical approximations will be useful for me across many areas of applied mathematics.

To see more details, check out the [paper]({{< paper "edu/pde-1.pdf" >}}).

Disclaimer: This project was completed as part of my BSc in Mathematics at Manchester Metropolitan University. The project was supervised by [Dr. Jon Shiach](https://www.mmu.ac.uk/staff/profile/dr-jon-shiach)
This blog post is an LLM generated text, based upon the hand written report.