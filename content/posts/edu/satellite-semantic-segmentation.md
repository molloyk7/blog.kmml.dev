---
title: "Satellite Semantic Segmentation"
date: 2021-05-12T09:44:00Z
draft: false
image: brand_image.jpg
tags: ["deep", "image", "semantic", "segmentation", "classification", "python", "arcgis"]
series: "msc"
---

I recently worked on an interesting project using deep learning for semantic segmentation of multispectral satellite images. The goal was to accurately categorize each pixel into one of 10 different classes like buildings, roads, trees, water bodies, vehicles etc. This allows us to create detailed maps from the imagery. The dataset came from the [DSTL Satellite Imagery Feature Detection challenge on Kaggle](https://www.kaggle.com/c/dstl-satellite-imagery-feature-detection).

Since this is a kaggle competition, its worth adding references/inspiration before the main text. 
- I was inspired by [this kernel](https://www.kaggle.com/lopuhin/full-pipeline-demo-poly-pixels-ml-poly)
- and [this kernel](https://www.kaggle.com/code/alijs1/squeezed-this-in-successful-kernel-run/script) for the U-Net architecture.
- As well as this [medium post](https://medium.com/thecyphy/dstl-satellite-imagery-feature-detection-f186048de52e) for walking me through concepts.
- And a [leaderboard topping approach](https://deepsense.ai/deep-learning-for-satellite-imagery-via-image-segmentation/)

## Dealing with Sparse Satellite Data

One of the first challenges was that the training dataset only consisted of 25 labeled satellite images covering various land areas. With deep learning models being notoriously data hungry, I was worried these 25 images might be too sparse to properly train a model. Each image was also captured in 20 different spectral bands at varying resolutions making them densely packed with color information - while great for classification, hard to directly feed to a convolutional neural network.

To overcome the limited data, I explored techniques like pansharpening to merge high resolution panchromatic images with lower resolution multispectral bands. This increased the level of detail available for training. I also calculated various vegetation and water indices using different spectral bands to quickly classify landscape regions into vegetation and water bodies. This simplified the problem space for the main semantic segmentation model.

## Training a U-Net Architecture

![unet architecture](/images/edu/satellite-semantic-segmentation/unet-architecture.png)

For semantic segmentation, I decided to use a U-Net architecture. Originally built for biomedical image segmentation, U-Nets work well even with small datasets by supplementing a typical convolutional neural network with successive upsampling layers. This allows combining high resolution features from early layers with upsampled output to precisely segment images.

I trained the U-Net for 50 epochs using the Adam optimizer, binary cross entropy loss and a Jaccard similarity coefficient that evaluates segmentation accuracy. Extensive data augmentation was done by randomly cropping patches from the 25 images and applying transformations.

## Promising Results on Satellite Segmentation

![object view](/images/edu/satellite-semantic-segmentation/object-single.png)

The results were quite positive, with a score of 0.40 on the competition leaderboard. The model performed especially well in detecting buildings, crops and waterways thanks to the preprocessed indices. Performance for vehicles was weaker, likely due to insufficient training examples.

Overall this project demonstrates that with the right techniques, deep learning can unlock detailed semantic maps from even sparse multispectral satellite data. By combining U-Nets with spectral preprocessing, I have a framework that can generalized to segmentation tasks on other satellite datasets. My next steps are to expand the training data and experiment with extracting individual objects. Exciting times ahead!

To see more details, check out the [paper]({{< paper "edu/lec402-2.pdf" >}}).

Further Reading:
[Fully Convolutional Networks for Semantic Segmentation](https://arxiv.org/abs/1411.4038)
[U-Net: Convolutional Networks for Biomedical Image Segmentation](https://arxiv.org/abs/1505.04597)

Disclaimer: This project was completed as part of my MSc in Data Science Lancaster University.
This blog post is an LLM generated text, based upon the hand-written report.