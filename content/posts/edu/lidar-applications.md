---
title: "Reviewing LiDAR for Road Applications"
date: 2021-03-18T11:59:00Z
draft: false
image: brand_image.jpg
tags: ["lidar", "classification", "python"]
series: "msc"
---

LiDAR (Light Detection and Ranging) is rapidly emerging as a preferred sensing modality for autonomous vehicles and infrastructure mapping. By firing laser pulses and analyzing reflected light, LiDAR scanners produce intricate 3D point clouds depicting roadways and surroundings in unprecedented detail. As this remote sensing technology becomes more ubiquitous, the bottleneck is now interpreting massive volumes of 3D data. This is where artificial intelligence comes in.

![lancaster university LiDAR pointcloud](/images/edu/lidar-applications/lancaster-pointcloud.png)
In this post, I’ll highlight promising machine learning techniques that could soon automate detection and evaluation of key road attributes for smarter cars and safer infrastructure.

## Learning to Segment the Driveable Surface

A foundation for many analysis tasks is properly differentiating drivable road from surrounding terrain. The latest neural networks show immense promise for semantic segmentation of LiDAR point clouds. Architectures like KPConv adapted from materials science now reach 95% IoU extracting ground layers. With a bit more work on degraded markings at the road edges, I anticipate near-perfect ML-based surface extraction within a year.

## Tracing Lane Geometry with Deep Learning
Mapping lane geometry including boundaries, curbs, and markings provides vital spatial awareness for self-driving vehicles. While classic computer vision handles camera images, LiDAR supplies 3D structural context. A recent Nature paper demonstrates astounding performance using graph neural networks to trace lane topology and extract centroids straight from point cloud sections. Such deep learning advancements can make lane-keeping and hazard avoidance far more reliable.

## Signaling the Future with Object Detection
Reading traffic signs and signals remains an unpredictable challenge plaguing autonomous navigation. But revived neural types like capsule networks show untapped potential for picking out posts, lights, and signage from LiDAR scans. Dynamic routing mechanisms can home in on distinctive shapes amidst complex urban infrastructure. I foresee capsule and transformer architectures enabling real-time status characterization at over 95% accuracy - a breakthrough for communicating intent to smart cars.

## Reimagining Road Design with Machine Learning
While vehicle autonomy garners greater attention, applying ML to LiDAR point clouds could also reinvent infrastructure planning and maintenance. The residual networks that power semantic segmentation, if trained on design metrics, might map cross-slopes, embankments, and guardrails better than any human surveyor. I also envision generative networks that propose improvements tailored to terrain, traffic patterns and crash data. LiDAR paired with deep learning may not just navigate roads, but radically redefine them.

The rapid pace of innovation gives me confidence that AI will soon unlock LiDAR’s immense potential. I plan to push my team towards developing production-ready ML solutions that usher in smarter, safer transportation infrastructure. Please reach out if you see promising research worth collaborating on!

To see more details, check out the [paper]({{< paper "edu/lec402-1.pdf" >}}).

Disclaimer: This project was completed as part of my MSc in Data Science Lancaster University.
This blog post is an LLM generated text, based upon the hand-written report.