---
title: "AOC 2023 Planning: Looking ahead in rust"
date: 2023-11-28T20:23:42Z
draft: false
image: brand_image.jpg
tags: ["aoc", "rust"]
series: "aoc"
---

[Last year I took my first run at Advent of Code using C++](/posts/aoc/2022/overall-thoughts/), managing to earn 14 stars over 7 days before tapping out. While a valuable learning experience, it showed me I still have room to grow as a programmer. This year I plan to attempt the challenges in Rust to expand my skills in a different systems language.
 
My goal is to better my overall performance from last year. While I don't expect to become a Rust expert overnight, its reputation for better memory safety makes me hopeful I'll avoid some of the tricky debugging C++ required. Rust's enum and pattern matching features should also help in parsing the complex AoC inputs and concisely modeling solutions.

Another benefit is Rust's ergonomics that I think will support my progress relative to C++. The cargo system and included testing frameworks will assist in setting up and validating solutions. And Rust's error messages - while still complex - are renowned for usefulness compared to the often confusing C++ compile errors I battled daily. The language and tooling seem optimized for programmer productivity.

Even more, I'm motivated to learn Rust because it shows promise for the future of performance-critical code like machine learning. While most ML libraries currently depend on C++, Rust is gaining traction as it balances speed and safety. As ML models and data grow exponentially, Rust's ability to optimize and parallelize workloads will only become more relevant. Getting proficient now in Advent of Code creates a foundation for me to contribute to the ML field's shift toward Rust.

I know Rust won't eliminate all the challenges that stump me during Advent of Code. But its modern designs and focus on performance and safety lead me to believe I can make greater headway compared to last year. Participating again in AoC will undoubtedly expand not just my Rust abilities, but problem solving and computational thinking skills. Here's hoping my next post will celebrate even more stars earned with Rust!