---
title: "AOC 2022 Recap: A Beginner C++ Journey"
date: 2022-12-27T20:34:56Z
draft: false
image: brand_image.jpg
tags: ["aoc", "c++"]
series: "aoc"
---

For those not familiar with [advent of code](https://adventofcode.com/2022/about):
> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as interview prep, company training, university coursework, practice problems, a speed contest, or to challenge each other.

I, as do many others, find it a great experience to reinforce problem solving and algorithm design, I especially like using it to reinforce new langauges, and thinking in them.

This year I decided to take on the Advent of Code programming challenges in C++. As someone still new to C++, it proved to be an interesting test of my skills. I managed to reach Day 7 before tapping out - but the experience gave me valuable practice and showed me areas I still need to improve.

I went into AoC fairly confident in my basic C++ knowledge - things like loops, functions, and basic data structures. The beginning days covering these basics were smooth sailing and I earned my first stars easily. But soon the complexity ramped up quickly and my limited understanding of more advanced concepts like templates and STL algorithms led to getting stuck. After wrestling for hours with Day 5 on parsing complex input, I realized I was still a coding novice in many ways.
 
Overall through 7 days and 14 stars I gained solid reinforcement of core programming ideas that will help me continue to improve. My eyes were also opened however to higher level C++ functionality I have much more to learn. Especially coming from languages like Python, the need for more manual memory management and knowing data structures like sets was an adjustment. I also found myself still thinking procedurally versus taking full advantage of OOP.

In the end, pushing myself out of my comfort zone with C++ and Advent of Code was hugely worthwhile. I added new tools to my coding toolkit and identified specific areas needing further study like STL features. The challenges were tough for a beginner but made me eager to continue advancing my C++ skills and mastery of computer science concepts. I may not have solved them all, but I'm walking away with a renewed passion for programming - plus 14 hard-earned stars!
 
Hopefully more stars next year!