---
title: "Leveling Up Cloud Architecture Skills as an ML Engineer"
date: 2023-12-01T22:37:31Z
draft: false
image: brand_image.jpg
tags: ["aws", "exam"]
---

Previous post: [Passing the AWS Certified Cloud Practitioner exam: An ML Engineers perpsective](/posts/rambles/aws-clf-c02-post-exam-thoughts.md)  
Validation: [Credly](https://www.credly.com/badges/bbc5fe8a-8798-44ac-9b37-af9d7e3168d6/public_url)

As a machine learning engineer, I recently passed the AWS Certified Solutions Architect Associate certification to expand my cloud architectural competencies. Having worked primarily in S3, EC2, and SageMaker, I knew this exam would force me to stretch into less familiar territory critical for effective ML solutions architecture. And it certainly delivered on that front through reinforced learnings, new discoveries, and expanded awareness.

Firstly, it solidified foundational capabilities like VPCs, IAM roles, security groups, S3 storage tiers, and database services – all centrally important for securing, accessing, and managing ML data and models. But it framed these through an architectural lens like properly decoupling storage from compute and implementing least-privilege access controls.

Additionally, I discovered a slew of capabilities I was completely unaware of but that have perfectly applicability for ML workflows. Services like SWF for managing complex stateful, long-running pipelines, EventBridge for gaining insights from application events, and Systems Manager for fleet monitoring and maintenance. The exam really highlighted the breadth of specialized tools AWS now offers.

And while not directly in the ML sphere, diving into topics like hybrid cloud, VPNs, transit gateways, and Direct Connect expanded my knowledge into how ML systems interconnect with internal infrastructure. This has already proven useful in discussions with my networking peers to bridge the gap between our spheres of influence.

But perhaps most critically, this exam enforced a high-level mental map of how the myriad ML services – SageMaker, Rekognition, Transcribe, Comprehend, Forecast – integrate and intersect to deliver intelligent capabilities. Understanding what tasks each service excels at and how they combine cohesively is imperative for architects to build comprehensive AI into solutions.

So although certainly more demanding than the Cloud Practitioner, the Solutions Architect Associate delivered manifold rewards. Not just in credentials, but in strengthening cloud architecture skills specialized for ML, expanding awareness of complementary capabilities, and incorporating connectivity concepts that well-round any cloud professional. I’m certainly finding this enhanced competence already manifesting itself in impactful ways in my daily work.

I guess its onto the ML Certification now - or maybe another platform? Azure? GCP? I guess we will see.