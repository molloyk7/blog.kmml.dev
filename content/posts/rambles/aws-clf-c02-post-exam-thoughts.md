---
title: "Passing the AWS Certified Cloud Practitioner exam: An ML Engineers perpsective"
date: 2021-01-18T07:18:49Z
draft: false
image: brand_image.jpg
tags: ["aws", "exam"]
---

Validation: [Credly](https://www.credly.com/badges/0fb44617-3dfe-4bbe-a740-8722d4b69541/public_url)

## Post Exam Thoughts

As a machine learning engineer, passing the [AWS Certified Cloud Practitioner](https://aws.amazon.com/certification/certified-cloud-practitioner/) exam provided useful learnings around utilizing cloud services for ML workflows. I wanted to share key takeaways most relevant for ML engineers.

The exam covers a wide range of AWS services. For ML, access to GPU compute power is critical for training models - instance types like P3 and G4 provide this capability. Services like SageMaker simplify the process from notebooks to training clusters.

Additionally, S3 offers a durable home for largescale datasets while integrating tightly with SageMaker for model building. And capabilities like security groups, IAM, and VPCs are important for securing sensitive data and models.

While not directly covered, monitoring and MLOps are important complementary skills for ML engineers to deploy models to production. CloudWatch, SageMaker Debugger, and tools like SageMaker Pipelines provide some solutions but require expertise to implement end-to-end.

Supplementing the Cloud Practitioner foundations with ML best practices in these areas is highly recommended. The certification also provided more fluency in AWS options, allowing me to make informed decisions on services for daily tasks.

And it empowered me to contribute insights around architectural discussions for our ML platforms. Though not strictly an ML exam, it delivered manifold benefits - improving my work, entrenching reliability best practices, and enhancing design conversations.

For any technical role working with ML, I'd wholeheartedly endorse the [Certified Cloud Practitioner](https://aws.amazon.com/certification/certified-cloud-practitioner/) exam to skill up on leveraging the breadth and depth of AWS services. With the acceleration of new capabilities, it's an efficient way to stay cloud-smart.

Onto the [Solutions Architect Associate](https://aws.amazon.com/certification/certified-solutions-architect-associate/) exam next! or maybe the [Machine Learning](https://aws.amazon.com/certification/certified-machine-learning-specialty/) exam?

## Resources Used:
- [Pluralsight course: AWS Certified Cloud Practitioner (CLF-C01) Path](https://www.pluralsight.com/paths/aws-certified-cloud-practitioner-clf-c01)

## Exam Thoughts

The exam itself was rather stresful, being my first online exam and not knowing what to expect. Especially when showing the proctor my room to prove i was not attempting to cheat.
The questions were quite simplistic, and as of taking the exam, i had nearly 3 years experience with AWS on a day to day basis. I would say that the exam is a good starting point for anyone looking to get into AWS, but if you have experience, only do it if you really want to - I found it a nice stepping stone.

